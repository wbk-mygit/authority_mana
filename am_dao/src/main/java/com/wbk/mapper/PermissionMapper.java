package com.wbk.mapper;

import com.wbk.domain.Permission;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface PermissionMapper {
    @Select("select * from permission where id in (select permissionId from role_permission where roleId=#{roleId})")
    public List<Permission> findByRoleId(String roleId);

    @Select("select * from permission")
    public List<Permission> findAll();

    @Insert("insert into permission(permissionname,url) values(#{permissionName},#{url})")
    public void save(Permission permission);

    @Select("select * from permission where id not in (select permissionId from role_permission where roleId=#{roleId})")
    public List<Permission> findOtherPermission(String roleId);
}
