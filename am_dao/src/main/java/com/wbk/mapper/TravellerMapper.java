package com.wbk.mapper;

import com.wbk.domain.Traveller;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface TravellerMapper {
    @Select("select * from traveller where id in (select travellerid from order_traveller where orderid=#{id})")
    public List<Traveller> findByOrdersId(String id);
}
