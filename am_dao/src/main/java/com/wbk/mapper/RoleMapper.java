package com.wbk.mapper;

import com.wbk.domain.Role;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface RoleMapper {
    @Select("select * from role where id in (select roleId from users_role where userId=#{userId})")
    @Results({
            @Result(id = true,property = "id",column = "id"),
            @Result(property = "roleName",column = "roleName"),
            @Result(property = "roleDesc",column = "roleDesc"),
            @Result(property = "permissions",column = "id",javaType = List.class,many = @Many(select = "com.wbk.mapper.PermissionMapper.findByRoleId"))
    })
    public List<Role> findByUserId(String userId);

    @Select("select * from role")
    public List<Role> findAll();

    @Insert("insert into role(roleName,roleDesc) values(#{roleName},#{roleDesc})")
    public void save(Role role);

    @Select("select * from role where id not in (select roleId from users_role where userId=#{userId})")
    public List<Role> findOtherRole(String userId);

    @Select("select * from role where id=#{roleId}")
    public Role findById(String roleId);

    @Insert("insert into role_permission values(#{permissionId},#{roleId})")
    public void addPermissionToRole(@Param("permissionId") String permissionId,@Param("roleId") String roleId);
}
