package com.wbk.mapper;

import com.wbk.domain.Product;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ProductMapper {

    @Select("select * from product where id=#{id}")
    public Product findProById(String id);

    /**
     * 查询全部商品
     * @return
     */
    @Select("select * from product")
    public List<Product> findAll();

    /**
     * 添加商品
     * @param product
     */
    @Insert("insert into product(productNum,productName,cityName,departureTime,productPrice,productDesc,productStatus) " +
            "values(#{productNum},#{productName},#{cityName},#{departureTime},#{productPrice},#{productDesc},#{productStatus})")
    public void save(Product product);
}
