package com.wbk.mapper;

import com.wbk.domain.Member;
import com.wbk.domain.Orders;
import com.wbk.domain.Product;
import org.apache.ibatis.annotations.*;
import org.springframework.core.annotation.Order;

import java.util.List;

public interface OrdersMapper {

    @Select("select * from orders")
    @Results({
            @Result(id = true,property = "id",column = "id"),
            @Result(property = "orderNum",column = "orderNum"),
            @Result(property = "orderTime",column = "orderTime"),
            @Result(property = "orderStatus",column = "orderStatus"),
            @Result(property = "peopleCount",column = "peopleCount"),
            @Result(property = "payType",column = "payType"),
            @Result(property = "orderDesc",column = "orderDesc"),
            @Result(property = "product",column = "productid",javaType = Product.class,one =@One(select = "com.wbk.mapper.ProductMapper.findProById"))
    })
    public List<Orders> findAll();

    @Select("select * from orders where id=#{id}")
    @Results({
            @Result(id = true,property = "id",column = "id"),
            @Result(property = "orderNum",column = "orderNum"),
            @Result(property = "orderTime",column = "orderTime"),
            @Result(property = "orderStatus",column = "orderStatus"),
            @Result(property = "peopleCount",column = "peopleCount"),
            @Result(property = "payType",column = "payType"),
            @Result(property = "orderDesc",column = "orderDesc"),
            @Result(property = "product",column = "productid",javaType = Product.class,one =@One(select = "com.wbk.mapper.ProductMapper.findProById")),
            @Result(property = "member",column = "memberId",javaType = Member.class,one = @One(select = "com.wbk.mapper.MemberMapper.findById")),
            @Result(property = "travellers",column = "id",javaType = List.class,many = @Many(select = "com.wbk.mapper.TravellerMapper.findByOrdersId"))
    })
    public Orders findById(String id);
}
