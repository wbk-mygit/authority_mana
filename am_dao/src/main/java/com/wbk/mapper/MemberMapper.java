package com.wbk.mapper;

import com.wbk.domain.Member;
import org.apache.ibatis.annotations.Select;

public interface MemberMapper {
    @Select("select * from member where id=#{id}")
    public Member findById(String id);
}
