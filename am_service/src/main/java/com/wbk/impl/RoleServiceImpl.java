package com.wbk.impl;

import com.wbk.RoleService;
import com.wbk.domain.Permission;
import com.wbk.domain.Role;
import com.wbk.mapper.PermissionMapper;
import com.wbk.mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public List<Role> findAll() {
        return roleMapper.findAll();
    }

    @Override
    public void save(Role role) {
        roleMapper.save(role);
    }

    @Override
    public List<Role> findOtherRole(String userId) {
        return roleMapper.findOtherRole(userId);
    }

    @Override
    public Role findById(String roleId) {
        return roleMapper.findById(roleId);
    }

    @Override
    public List<Permission> findOtherPermission(String roleId) {
        return permissionMapper.findOtherPermission(roleId);
    }

    @Override
    public void addPermissionToRole(String roleId, String[] permissionIds) {
        for (String permissionId:permissionIds){
            roleMapper.addPermissionToRole(permissionId,roleId);
        }
    }
}
