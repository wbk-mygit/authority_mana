package com.wbk.impl;

import com.wbk.ProductService;
import com.wbk.domain.Product;
import com.wbk.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.List;
@Service
@Transactional
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductMapper productMapper;
    @Override
    public List<Product> findAll() {
        return productMapper.findAll();
    }

    @Override
    public void save(Product product) throws ParseException {
//        String uuid = UuidUtil.getUuid();
//        product.setId(uuid);
        productMapper.save(product);
    }
}
