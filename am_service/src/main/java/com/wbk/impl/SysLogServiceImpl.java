package com.wbk.impl;

import com.wbk.SysLogService;
import com.wbk.domain.SysLog;
import com.wbk.mapper.SysLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SysLogServiceImpl implements SysLogService {

    @Autowired
    private SysLogMapper sysLogMapper;
    @Override
    public void save(SysLog sysLog) {
    sysLogMapper.save(sysLog);
    }

    @Override
    public List<SysLog> findAll() {
        return sysLogMapper.findAll();
    }
}
