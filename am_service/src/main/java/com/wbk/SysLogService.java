package com.wbk;

import com.wbk.domain.SysLog;

import java.util.List;

public interface SysLogService {
    public void save(SysLog sysLog);
    public List<SysLog> findAll();
}
