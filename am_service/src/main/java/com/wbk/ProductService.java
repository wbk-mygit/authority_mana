package com.wbk;

import com.wbk.domain.Product;

import java.text.ParseException;
import java.util.List;

public interface ProductService {
    public List<Product> findAll();
    public void save(Product product) throws ParseException;
}
