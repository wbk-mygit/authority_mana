package com.wbk;

import com.wbk.domain.Permission;
import com.wbk.domain.Role;

import java.util.List;

public interface RoleService {

    public List<Role> findAll();

    public void save(Role role);

    public List<Role> findOtherRole(String userId);

    public Role findById(String roleId);

    public List<Permission> findOtherPermission(String roleId);

    public void addPermissionToRole(String roleId,String[] permissionIds);
}
