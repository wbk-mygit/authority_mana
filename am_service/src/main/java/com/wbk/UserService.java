package com.wbk;

import com.wbk.domain.UserInfo;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
public interface UserService extends UserDetailsService {

    public List<UserInfo> findAll();

    public void save(UserInfo userInfo);

    public UserInfo findById(String id);

    public void addRoleToUser(String userId,String[] roleIds);
}
