package com.wbk.controller;

import com.wbk.SysLogService;
import com.wbk.domain.SysLog;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;

@Component
@Aspect
public class LogAop {

    @Autowired
    private SysLogService sysLogService;
    private Date visitTime;//访问时间
    private Class clazz;//访问的类
    private Method method;//访问的方法

    @Autowired
    private HttpServletRequest request;

    @Before("execution(* com.wbk.controller.*.*(..))")
    public void doBefore(JoinPoint jp) throws NoSuchMethodException {
        //获取访问时间
        visitTime=new Date();
        //获取访问的类
        clazz=jp.getTarget().getClass();
        //获取访问的方法
        String name = jp.getSignature().getName();//获取访问的方法名
        Object[] args = jp.getArgs();//获取目标方法的参数
        if (args==null||args.length==0){//根据有无参数获取方法
            method = clazz.getMethod(name);
        }else {
            Class[] classes=new Class[args.length];
            for (int i=0;i<args.length;i++){
                classes[i]=args[i].getClass();
            }
            method=clazz.getMethod(name,classes);
        }

    }

    @After("execution(* com.wbk.controller.*.*(..))")
    public void doAfter(JoinPoint jp){
        //获取访问时长
        long executionTime=new Date().getTime()-visitTime.getTime();
        //获取访问url
        String url="";
        if (clazz!=null&&method!=null&&clazz.getName()!="LogAop"){
            RequestMapping classAnnotation = (RequestMapping) clazz.getAnnotation(RequestMapping.class);
            String[] classValue = classAnnotation.value();
            RequestMapping methodAnnotation = method.getAnnotation(RequestMapping.class);
            String[] methodValue = methodAnnotation.value();
            url=classValue[0]+methodValue[0];
        }
        //获取访问IP
        String ip = request.getRemoteAddr();
        //获取操作的用户名
        SecurityContext securityContext = SecurityContextHolder.getContext();
        User user =(User) securityContext.getAuthentication().getPrincipal();
        String username = user.getUsername();
        //封装syslog对象
        SysLog sysLog=new SysLog();
        sysLog.setExecutionTime(executionTime);
        sysLog.setIp(ip);
        sysLog.setUrl(url);
        sysLog.setUsername(username);
        sysLog.setVisitTime(visitTime);
        sysLog.setMethod("[类名]"+clazz.getName()+"[方法名]"+method.getName());
        //调用service存储
        if (clazz.getName()!="com.wbk.controller.SysLogController"){
            sysLogService.save(sysLog);
        }
    }
}
