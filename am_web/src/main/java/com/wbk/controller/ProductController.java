package com.wbk.controller;

import com.wbk.ProductService;
import com.wbk.domain.Product;
import com.wbk.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.RolesAllowed;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private ProductService productService;


    @RequestMapping("/findAll.do")
    public ModelAndView findAll(){
        ModelAndView mv=new ModelAndView();
        List<Product> productList = productService.findAll();
        mv.setViewName("product-list");
        mv.addObject("productList",productList);
        return mv;
    }

    @RequestMapping("/save.do")
    public String save(Product product) throws ParseException {
        productService.save(product);
        System.out.println(product.toString());
        return "redirect:/product/findAll.do";
    }
}
