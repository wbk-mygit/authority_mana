# Authority_mana

#### 介绍
基于SSM架构的企业权限控制系统

#### 技术选型
- 架构：spring+springMVC+mybatis+springSecurity
- 前端：html+css+javascript+jQuery+AdminLTE
- 数据库：oracle
- 开发工具：idea+maven+git



#### 实现的需求

- 查询和添加产品
- 查询订单
- 使用springSecurity登录和注销
- 查询和添加用户，角色，权限
- 对不同的用户进行权限控制
- AOP日志记录用户操作



