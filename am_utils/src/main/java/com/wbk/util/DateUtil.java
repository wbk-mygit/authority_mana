package com.wbk.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

//    日期转字符串
    public static String date2String(Date date, String ptt){
        SimpleDateFormat sdf=new SimpleDateFormat(ptt);
        String s = sdf.format(date);
        return s;
    }

//    字符串转入日期
    public static Date string2Date(String s,String ptt) throws ParseException {
        SimpleDateFormat sdf=new SimpleDateFormat(ptt);
        Date parse = sdf.parse(s);
        return parse;
    }
}
