package com.wbk.util;


import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BCryptPasswordEncoderUtil {
    private static BCryptPasswordEncoder bCryptPasswordEncoder=new BCryptPasswordEncoder();

    public static String encode(String password){
        String s = bCryptPasswordEncoder.encode(password);
        return s;
    }

}
